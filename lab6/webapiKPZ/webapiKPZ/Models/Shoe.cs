﻿namespace webapiKPZ.Models
{
    public class Shoe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Size { get; set; }
        public double Price { get; set; }
    }
}
