﻿using Microsoft.EntityFrameworkCore;

namespace webapiKPZ.Models
{
    public class ApplicationContext : DbContext
    {
        public virtual DbSet<Shoe> Shoes { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        public ApplicationContext()
        {

        }
    }
}
