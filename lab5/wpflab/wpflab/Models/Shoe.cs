﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace wpflab.Models
{
    public class Shoe : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        private string _name;
        private int _size;
        private Season _season;
        public string Name { 
            get => _name; 
            set 
            {
                _name = value;
                OnPropertyChanged("Name");
            } 
        }
        public int Size { 
            get => _size; 
            set
            {
                _size = value;
                OnPropertyChanged("Size");
            }
        }
        public Season Season { 
            get => _season;
            set
            {
                _season = value;
                OnPropertyChanged("Season");
            }
        }
    }
}
