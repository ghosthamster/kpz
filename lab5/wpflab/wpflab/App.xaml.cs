﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using wpflab.ViewModels;

namespace wpflab
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ApplicationViewModel _model;
        public App()
        {
            _model = new ApplicationViewModel();
            var window = new MainWindow() { DataContext = _model };
            window.Show();
        }
    }
}
