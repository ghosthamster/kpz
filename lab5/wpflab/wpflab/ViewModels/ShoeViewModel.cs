﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using wpflab.Models;

namespace wpflab.ViewModels
{
    class ShoeViewModel : INotifyPropertyChanged
    {
        private Shoe _shoe;
        public ShoeViewModel(Shoe shoe)
        {
            _shoe = shoe;
        }
        public string Name
        {
            get { return _shoe.Name; }
            set
            {
                _shoe.Name = value;
                OnPropertyChanged("Name");
            }
        }
        public int Size
        {
            get { return _shoe.Size; }
            set
            {
                _shoe.Size = value;
                OnPropertyChanged("Size");
            }
        }
        public Season Season
        {
            get { return _shoe.Season; }
            set
            {
                _shoe.Season = value;
                OnPropertyChanged("Season");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
