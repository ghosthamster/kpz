﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Data;
using wpflab.Commands;
using wpflab.Models;

namespace wpflab.ViewModels
{
    class ApplicationViewModel : INotifyPropertyChanged
    {

        private Command _addCommand;
        public Command AddCommand
        {
            get
            {
                return _addCommand ??
                  (_addCommand = new Command(obj =>
                  {
                      Shoe shoe = new Shoe();
                      Shoes.Insert(0, shoe);
                      SelectedShoe = shoe;
                  }));
            }
        }

        public ObservableCollection<Shoe> Shoes { get; set; }
        private Shoe _selectedShoe;
        public Shoe SelectedShoe
        {
            get { return _selectedShoe; }
            set
            {
                _selectedShoe = value;
                OnPropertyChanged("SelectedShoe");
            }
        }

        public ApplicationViewModel()
        {
            Shoes = new ObservableCollection<Shoe>
            {
                new Shoe { Name="Some shoe", Size=42, Season=Season.Autumn },
                new Shoe { Name="Some shoe2", Size=41, Season=Season.Winter },
                new Shoe { Name="Cool shoe", Size=40, Season=Season.Autumn },
                new Shoe { Name="Text", Size=31, Season=Season.Spring }
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }




        public class IntToStringConverter : IValueConverter, IMultiValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
              => (string)parameter == "plusOne" ? ((int)value + 1).ToString() : value.ToString();

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
              => throw new NotImplementedException();

            public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
             => $"{Convert(values[0], targetType, parameter, culture) as string} {values[1]}";

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
              => throw new NotImplementedException();
        }
    }
}
