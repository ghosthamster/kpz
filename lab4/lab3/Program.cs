﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lab3
{
    class Program
    {
        public static Player player;
        public static Shop shop;
        private static void ChangeMoney(double money)
        {
            if(money < 100)
                Console.WriteLine($"You have only {money}$, stop shoping!!!");
        }
        private static void NoMoney()
        {
                Console.WriteLine($"YOU HAVE 0 MONEY!!!!");
        }
        static void Main(string[] args)
        {
            player = new Player();
            shop = new Shop();
            var items = new List<Item>();
            items.Add(new Item { name = "me4", price = 5 });
            items.Add(new Item { name = "syper me4", price = 10 });
            items.Add(new Item { name = "pyper me4", price = 20 });
            items.Add(new Item { name = "mega me4", price = 15 });
            items.Add(new Item { name = "kk me4", price = 16 });
            items.Add(new Item { name = "kill me4", price = 100 });
            shop.items = new List<Item>(items);
            

            //menu
            while (true)
            {
                Console.WriteLine("Menu" + Environment.NewLine +
                "1.See info" + Environment.NewLine +
                "2.Buy item" + Environment.NewLine +
                "3.Filter items" + Environment.NewLine +
                "4.Sort items by name" + Environment.NewLine +
                "5.Subscribe" + Environment.NewLine +
                "6.UNsubscribe");
                var chose = Console.ReadLine();
                switch (chose)
                {
                    case "1":
                        printInfo();
                        break;
                    case "2":
                        Console.WriteLine("Enter item name");
                        var name = Console.ReadLine();
                        var item = shop.items.Where(x=>x.name == name).FirstOrDefault();
                        if(item != null && item.price <= player.money)
                        {
                            player.items.Add(item);
                            shop.items.Remove(item);
                            player.money -= item.price;
                            Console.WriteLine("Ok");
                        }
                        else
                        {
                            Console.WriteLine("No!");
                        }
                        
                        break;
                    case "3":
                        Console.WriteLine("Enter max price");
                        var price = Convert.ToDouble(Console.ReadLine());
                        var nodic = shop.items.Where(x => x.price < price);
                        Dictionary<double, string> dic = new Dictionary<double, string>();
                        foreach(var i in nodic)
                        {
                            dic.Add(i.price, i.name);
                        }
                        printInfo(dic);
                        break;
                    case "4":
                        var sorted = new { shop = shop.items.OrderBy(x => x.name).ToList<Item>(), player = player.items.OrderBy(x => x.name).ToList<Item>() };
                        shop.items = sorted.shop.AsQueryable().Select(x=>x).ToList<Item>();
                        player.items = sorted.player;
                        printInfo();
                        break;
                    case "5":
                        player.Changed += ChangeMoney;
                        player.Nomoney += NoMoney;
                        break;
                    case "6":
                        player.Changed -= ChangeMoney;
                        player.Nomoney -= NoMoney;
                        break;
                    case "7":
                        var it = shop.items.GroupBy(x => x.price < 15).SelectMany(x => x).ToList();
                        foreach (var i in it)
                        {
                            Console.WriteLine(i.ToString());
                        }
                        break;
                    default:
                        Console.WriteLine("Wrong number");
                        break;
                }
            }
            

        }
        static void Minus(ref double x,double y)
        {
            x -= y;
        }
        static void Add(ref double x, double y, out double res)
        {
            res = x + y;
        }
        static void printInfo()
        {
           Console.WriteLine("Shop items:" + shop.ToString());
            Console.WriteLine("Player items:" + player.ToString());
        }
        static void printInfo(Dictionary<double,string> dic)
        {
            string ret = "";
            ret += "Items:" + Environment.NewLine;
            foreach (var item in dic)
            {
                ret += item.Value + "  " + item.Key + Environment.NewLine;
            }
            Console.WriteLine(ret);
        }
        
    }
    public static class StringExtension
    {
        public static int CharCount(this string str, char c)
        {
            int counter = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    counter++;
            }
            return counter;
        }
    }
}
