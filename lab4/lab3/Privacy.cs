﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab3
{
    public class SuperPlayer
    {
        void Play()
        {
            Console.WriteLine("Start playing");
        }
        void StopPlay()
        {
            Console.WriteLine("Stop playing");
        }
        internal class info
        {
            private Inform inform { get; set; } = new Inform() { inf="someinfo"};
            private class Inform
            {
                public string inf { get; set; }
            }
        }
        protected string passworod { get; set; }
        protected internal static void Test()
        {
            var slp = new SuperPlayer();
            slp.Play();
            slp.StopPlay();
            var inf = new info();
        }
    }
    /*class Test
    {
        public void t()
        {
            var slp = new SuperPlayer();
            slp.Play();
            slp.StopPlay();
            slp.passworod;
            slp.Test();
            var inf = new info();
        }
    }*/
    class Test
    {
        [Flags]
        enum ItemsCategory
        {
            sword = 0x01,
            pousion = 0x02,
            wear = 0x08
        }
        public static void T()
        {
            var categ1 = ItemsCategory.sword | ItemsCategory.pousion;
            var categ2 = ItemsCategory.sword & ItemsCategory.pousion;
            var categ3 = ItemsCategory.sword ^ ItemsCategory.pousion;
        }
    }
   

}