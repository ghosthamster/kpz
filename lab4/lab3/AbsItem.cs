﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;


namespace lab3
{
    public interface IItem
    {
        public string ToString();
    }
    public abstract class AbsItem : IItem
    {
        public abstract string name { get; set; }
        public abstract double price { get; set; }
        public abstract string ToString();
    }
    public class Item: AbsItem, IComparer<Item>
    {
        public Item(string name)
        {
            this.name = name;
        }
        public Item(string name,double price): this(name)
        {
            this.price = price;
        }
        public Item() : base()
        {

        }
        /*static Item()
        {
            double max = 1000;
            object m = max;
            maxprice = (double)m;
        }*/
        static Item()
        {
            int max = 1000;
            maxprice = (double)max;
            //or
            int maxx = 1000;
            maxprice = maxx;
        }
        public static double maxprice = 2000;
        public override string name { get; set; }
        public override double price { get; set; }
        public int Compare([AllowNull] Item x, [AllowNull] Item y)
        {
            return (x.name.CompareTo(y.name));
        }

        public override string ToString()
        {
            return name + "  " + price.ToString();
        }
        public override bool Equals(object? obj)
        {
             return this.GetHashCode() == obj?.GetHashCode();
        }
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }


    }
    
}
