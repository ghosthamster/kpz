﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab3
{
    public class Player
    {
        public delegate void ChangeMoneyEventHandler(double money);
        public event ChangeMoneyEventHandler Changed;
        public delegate void EventHandler();
        public event EventHandler Nomoney;
        public List<Item> items;
        private double _money;
        public double money { get { return _money; } set {
                _money = value;
                if(_money != 0)
                {
                    Changed?.Invoke(value);
                }
                else
                {
                    Nomoney?.Invoke();
                }
            } }
        public Player()
        {
            items = new List<Item>();
            money = 120;
        }
        public override string ToString()
        {
            string ret = "";
            ret += "Money:" + money + Environment.NewLine;
            ret += "Items:" + Environment.NewLine;
            foreach(var item in items)
            {
                ret += item.ToString() + Environment.NewLine;
            }
            return ret;
        }
    }
}
