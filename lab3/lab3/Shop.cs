﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab3
{
    class Shop
    {

        private List<Item> _items;
        public List<Item> items { get { return _items; } set {
                _items = value;
             
            } }
        public Shop()
        {
            items = new List<Item>();
        }
        public override string ToString()
        {
            string ret = "";
            ret += "Items:" + Environment.NewLine;
            foreach (var item in items)
            {
                ret += item.ToString() + Environment.NewLine;
            }
            return ret;
        }
    }
}
