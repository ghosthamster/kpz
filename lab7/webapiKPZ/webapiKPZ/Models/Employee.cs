﻿namespace webapiKPZ.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public double salary { get; set; }
        public string position { get; set; }
    }
}
