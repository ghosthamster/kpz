﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace lab3
{
    public class Item : IComparer<Item>
    {
        public string name;
        public double price;


        public int Compare([AllowNull] Item x, [AllowNull] Item y)
        {
            return (x.name.CompareTo(y.name));
        }

        public override string ToString()
        {
            return name + "  " + price.ToString();
        }
    }

}
